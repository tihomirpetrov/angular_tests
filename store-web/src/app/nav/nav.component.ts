import {Component, OnInit} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';
import {DatePipe} from '@angular/common';
import {Router} from '@angular/router';
import {LoginComponent} from '../login/login.component';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  title = 'Web Store';
  menuItems = ['dashboard', 'sales', 'orders', 'customers', 'products', 'admin'];
  currentDate: any = new Date();

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver,
              private datePipe: DatePipe,
              private router: Router,
              private loginComponent: LoginComponent) {
    this.currentDate = this.datePipe.transform(this.currentDate, 'dd.MM.yyyy HH:mm:ss');
  }

  ngOnInit(): void {
    setInterval(() => {
      this.currentDate = new Date();
      this.currentDate = this.datePipe.transform(this.currentDate, 'dd.MM.yyyy HH:mm:ss');
    }, 1000);
  }

  login() {
    this.router.navigateByUrl('/login');
    /*if (this.loginComponent.loading) {
      this.router.navigateByUrl('/admin');
    } else if (this.loginComponent.loading === false) {
      console.log('You are not autorized!');
    }*/
  }
}
