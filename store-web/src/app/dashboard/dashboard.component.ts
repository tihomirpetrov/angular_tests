import {Component, OnInit} from '@angular/core';

export interface Tile {
  color: string;
  cols?: number;
  rows?: number;
  text: string;
  icon?: string;
  url?: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  tiles: Tile[] = [
    {icon: 'point_of_sale', text: 'sales', url: '/sales', cols: 2, rows: 2, color: 'lightgreen'},
    {icon: 'storefront', text: 'products', url: '/products', cols: 2, rows: 2, color: 'lightblue'},
    {icon: 'admin_panel_settings', text: 'administration', url: '/admin', cols: 2, rows: 2, color: 'lightpink'},
    {icon: 'list_alt', text: 'orders', url: '/orders', cols: 2, rows: 2, color: '#DDBDF1'},
    {icon: 'dashboard_customize', text: 'customers', url: '/customers', cols: 2, rows: 2, color: 'lightgray'},
  ];

  constructor() {
  }

  ngOnInit(): void {
  }
}
