import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SalesComponent} from './sales/sales.component';
import {OrdersComponent} from './orders/orders.component';
import {CustomersComponent} from './customers/customers.component';
import {ProductsComponent} from './products/products.component';
import {LoginComponent} from './login/login.component';
import {AdminComponent} from './admin/admin.component';

const routes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  {path: 'sales', component: SalesComponent},
  {path: 'orders', component: OrdersComponent},
  {path: 'customers', component: CustomersComponent},
  {path: 'products', component: ProductsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'admin', component: AdminComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
