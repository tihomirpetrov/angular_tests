import {Component, OnInit} from '@angular/core';
import {Product} from '../models/product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.sass']
})
export class ProductsComponent implements OnInit {
  products: Product[] = [];
  product: Product = {
    id: 101,
    name: 'Test product',
    image: {
      url: 'https://picsum.photos/436/240/'
    },
    description: 'Some description here',
    status: 'active',
    price: 10,
  };
  constructor() {
  }

  ngOnInit(): void {
    // this.addProduct();
  }


  addProduct(): void {
    debugger;
    const item = this.product;
    this.products.push(item);
  }

  deleteProduct(index): void {
    this.products.splice(index, 1);
  }

  editProduct(index): void {
    if (this.products[index].status === 'active') {
      this.products[index].status = 'inactive';
    } else if (this.products[index].status === 'inactive') {
      this.products[index].status = 'active';
    }
  }

}
