import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';

export interface PeriodicElement {
  position: number;
  firstName: string;
  lastName: string;
  sum: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, firstName: 'Иван', lastName: 'Иванов', sum: 210.20},
  {position: 2, firstName: 'Петър', lastName: 'Петров', sum: 31.80},
  {position: 3, firstName: 'Пешо', lastName: 'Пешев', sum: 8.20},
  {position: 4, firstName: 'Антон', lastName: 'Иванов', sum: 66.50},
  {position: 5, firstName: 'Валентин', lastName: 'Тонев', sum: 10.80},
  {position: 6, firstName: 'Любомир', lastName: 'Христов', sum: 12.10},
  {position: 7, firstName: 'Георги', lastName: 'Андонов', sum: 14.60},
  {position: 8, firstName: 'Николай', lastName: 'Тодоров', sum: 15.90},
  {position: 9, firstName: 'Димитър', lastName: 'Димитров', sum: 18.40},
  {position: 10, firstName: 'Евгения', lastName: 'Пейчева', sum: 20.10},
];


@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.sass']
})
export class CustomersComponent implements OnInit {

  displayedColumns: string[] = ['position', 'firstName', 'lastName', 'sum'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  constructor() { }

  ngOnInit(): void {
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
