import {Injectable} from '@angular/core';
import {Product} from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class MockService {

  constructor() {
  }

  getItems(): Product[] {
    return [
      {id: 1, name: 'Product 01', image: {url: 'https://picsum.photos/436/240/'}, description: '', status: 'active', price: 1.20},
      {id: 2, name: 'Product 02', image: {url: 'https://picsum.photos/436/240/'}, description: '', status: 'active', price: 2.80},
      {id: 3, name: 'Product 03', image: {url: 'https://picsum.photos/436/240/'}, description: '', status: 'active', price: 10.00},
      {id: 4, name: 'Product 04', image: {url: 'https://picsum.photos/436/240/'}, description: '', status: 'active', price: 10.20},
      {id: 5, name: 'Product 05', image: {url: 'https://picsum.photos/436/240/'}, description: '', status: 'active', price: 20.00},
      {id: 6, name: 'Product 06', image: {url: 'https://picsum.photos/436/240/'}, description: '', status: 'active', price: 6.50},
      {id: 7, name: 'Product 07', image: {url: 'https://picsum.photos/436/240/'}, description: '', status: 'active', price: 3.25},
      {id: 8, name: 'Product 08', image: {url: 'https://picsum.photos/436/240/'}, description: '', status: 'active', price: 4.00},
    ];
  }
}
