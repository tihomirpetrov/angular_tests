import {Injectable} from '@angular/core';
import {EventEmitter} from 'events';
import {BehaviorSubject} from 'rxjs';
import {Product} from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class SalesService {
  orderSubject: BehaviorSubject<Array<Product>> = new BehaviorSubject<Array<Product>>([]);

  viewAddedOrder(value: Array<Product>): any {
    const currentValue = this.orderSubject.value;
    value.forEach(product => currentValue.push(product));
    this.orderSubject.next(currentValue);
  }
}
