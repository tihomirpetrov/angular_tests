import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PagesRoutingModule} from './pages-routing.module';
import { PagesComponent } from './pages.component';
import {DashboardModule} from '../dashboard/dashboard.module';
import {AdminModule} from '../admin/admin.module';
import {LoginModule} from '../login/login.module';
import {CustomersModule} from '../customers/customers.module';
import {ProductsModule} from '../products/products.module';
import {SalesModule} from '../sales/sales.module';



@NgModule({
  declarations: [PagesComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    DashboardModule,
    AdminModule,
    LoginModule,
    CustomersModule,
    ProductsModule,
    SalesModule
  ],
  exports: [PagesComponent]
})
export class PagesModule { }
