import {Component, Inject, Input, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {SalesService} from '../../services/sales.service';
import {SalesComponent} from '../sales.component';
import {Product} from '../../models/product';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.sass']
})
export class ViewOrderComponent implements OnInit {
  total: any;
  products: any[];

  constructor(public dialogRef: MatDialogRef<SalesComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {
    this.total = this.data.total;
    this.products = this.data.products;
  }

  addOrder(value: Array<Product>): void {
    this.dialogRef.close(value);
  }

}
