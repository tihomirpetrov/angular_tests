import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MockService} from '../services/mock.service';
import {Product} from '../models/product';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Image} from '../models/image';
import {FormBuilder, FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {ViewOrderComponent} from './view-order/view-order.component';
import {SalesService} from '../services/sales.service';

export interface ProductData {
  id: string;
  name: string;
  image?: Image;
  description: string;
  status: string;
  price: number;
}

/** Constants used to fill up our data base. */
const PRICE: number[] = [
  1.20, 2, 3, 4, 5, 6, 7, 8, 9, 10,
  2.40, 3.50, 6.10, 1.50, 4.20
];
const NAMES: string[] = [
  'Шампоан Dove 500 ml', 'Сапун Dove 100 g', 'Балсам Nivea 360 ml', 'Ariel прах за пране',
  'Skip oмекотител', 'Sagrasattore обезмаслител', 'Omo прах за пране - течен', 'Dash прах за пране', 'Nivea пяна за бръснене', 'Dermomed душ гел'
];

const PRODUCTS: any [] = [
  {
    id: 1,
    name: 'Шампоан Dove 500 ml',
    status: 'active',
    price: 3.20
  },
  {
    id: 2,
    name: 'Сапун Dove 100 g',
    status: 'active',
    price: 1.00
  },
  {
    id: 3,
    name: 'Балсам Nivea 360 ml',
    status: 'active',
    price: 4.50
  },
  {
    id: 4,
    name: 'Ariel прах за пране',
    status: 'active',
    price: 15.90
  },
  {
    id: 5,
    name: 'Skip oмекотител',
    status: 'active',
    price: 12.60
  },
  {
    id: 6,
    name: 'Sagrasattore обезмаслител',
    status: 'active',
    price: 5.40
  },
  {
    id: 7,
    name: 'Omo прах за пране - течен',
    status: 'active',
    price: 12.20
  },
  {
    id: 8,
    name: 'Dash прах за пране',
    status: 'active',
    price: 18.90
  },
  {
    id: 9,
    name: 'Nivea пяна за бръснене',
    status: 'active',
    price: 6.20
  },
  {
    id: 10,
    name: 'Dermomed душ гел',
    status: 'active',
    price: 4.90
  },
];

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.sass']
})
export class SalesComponent implements OnInit, AfterViewInit {
  dataSource: MatTableDataSource<ProductData>;
  myControl = new FormControl();
  options: Product[] = PRODUCTS;
  filteredOptions: Observable<string[]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  listProducts;
  addedProducts: Product[] = [];
  totalPrice = 0;
  value: any;
  valueQuantity: any;
  selectedOptions: number[] = [1, 2, 3, 4, 5];

  constructor(private mockService: MockService,
              public dialog: MatDialog,
              private salesService: SalesService,
              private fb: FormBuilder) {
    const users = Array.from({length: 100}, (_, k) => createNewProduct(k + 1));

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
  }

  ngOnInit(): void {
    this.listProducts = this.mockService.getItems();
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );

  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private _filter(value: any): any[] {
    const temp = [];
    const filterValue = value;

    return this.options.filter(option => option.name.indexOf(filterValue) === 0);
  }

  getOptionText(option): any {
    if (option != null) {
      return option = `${option.name} - ${option.price?.toFixed(2)} лв`;
    }
    return '';
  }

  addProducts(): any {
    const product: Product = {
      id: 101,
      name: 'Шампоан Dove 500 ml',
      image: {
        url: 'https://picsum.photos/436/240/'
      },
      description: 'Some description here',
      status: 'active',
      price: 10,
    };
    this.addedProducts.push(product);
    this.totalPrice += product.price;
  }

  deleteProduct(): any {
    const currentPrice = this.totalPrice;
    if (this.addedProducts.length > 1) {
      const tempPrice = this.addedProducts[this.addedProducts.length - 1].price;
      this.addedProducts.splice(this.addedProducts.length - 1, 1);
      this.totalPrice = currentPrice - tempPrice;
      return this.totalPrice;
    } else {
      this.addedProducts.splice(0, 1);
      this.totalPrice = 0;
    }

  }

  selectedProduct(event): any {
    this.addedProducts.push(event.option.value);
    this.totalPrice += event.option.value.price;
  }

  calculate(): any {
    const dialogRef = this.dialog.open(ViewOrderComponent, {
      width: '400px',
      data: {
        total: this.totalPrice,
        products: this.addedProducts,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      debugger;
      this.salesService.viewAddedOrder(result);
    });
  }

}

function createNewProduct(id: number): ProductData {
  const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
    NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    id: id.toString(),
    name: NAMES[Math.round(Math.random() * (NAMES.length - 1))],
    image: {
      url: ''
    },
    description: '',
    status: 'active',
    price: PRICE[Math.round(Math.random() * (PRICE.length - 1))],
  };
}
