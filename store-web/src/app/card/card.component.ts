import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../models/product';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input()
  _item;
  @Input()
  content;
  @Input() title;

  constructor() {
  }

  ngOnInit(): void {
  }

  get item(): any {
    this._item = {
      title: this.title,
      content: this.content,
    };
    return this._item;
  }

  set item(value: any) {
    debugger;
    this._item = value;
  }

  delete(item): any {
    debugger;
    let temp;
    temp = {
      title: '',
      content: ''
    };
    this._item = temp;
    this.item.splice(1, 1);
  }
}
