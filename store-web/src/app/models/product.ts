import {Image} from './image';

export class Product {
  id: number;
  name: string;
  image?: Image;
  description?: string;
  status?: 'active' | 'inactive';
  price: number;
}
