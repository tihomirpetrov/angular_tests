export class Image {
  size?: string;
  name?: string;
  type?: string;
  url: string;
}
