import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OrdersComponent} from './orders.component';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import {SalesModule} from '../sales/sales.module';
import {MatRippleModule} from '@angular/material/core';
import {ViewOrderComponent} from '../sales/view-order/view-order.component';



@NgModule({
  declarations: [OrdersComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
    TranslateModule,
    FormsModule,
    SalesModule,
    MatRippleModule
  ],
  exports: [OrdersComponent],
  entryComponents: [ViewOrderComponent]
})
export class OrdersModule { }
