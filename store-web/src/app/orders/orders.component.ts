import {Component, OnInit} from '@angular/core';
import {SalesService} from '../services/sales.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.sass']
})
export class OrdersComponent implements OnInit {
  completedOrders = [];
  totalOrdersPrice = 0;
  constructor(private salesService: SalesService) {
  }

  ngOnInit(): void {
    this.salesService.orderSubject.subscribe(result => {
      this.completedOrders = result;
      this.completedOrders.forEach(product => {
        debugger;
        this.totalOrdersPrice += product.price;
      });
      debugger;
    });
  }

}
