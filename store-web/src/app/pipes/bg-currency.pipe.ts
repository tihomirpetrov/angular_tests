import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bgCurrency'
})
export class BgCurrencyPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    args[0] = 'лв';
    if (value) {
      value = this.addZeroes(value) + ' ' + args[0];
    }
    return value;
  }

  addZeroes(value): any {
    if (typeof (value) !== 'string') {
      value = value.toString();
    }
    const item = value.split('.');
    if (item.length === 1 || item[1].length < 3) {
      value = parseFloat(value).toFixed(2);
    }

    return value;
  }

}
